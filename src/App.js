



import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './components/home'
import User from './components/user'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Route exact path='/' component={Home} />
          <Route path='/:user_id' component={ User } />
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
