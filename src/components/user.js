



import React, { Component } from 'react';
import axios from 'axios';

class User extends Component {

    state = {
        id: null,
        user:null
    }

    componentDidMount(){
        let id = this.props.match.params.user_id;
        axios.get('https://jsonplaceholder.typicode.com/users/' + id)
        .then(result => {
            this.setState({
                user: result.data
            })
            console.log(result.data)
        })



        this.setState({
            id:id
        })

    }

    render(){

        const user = this.state.user ? (
            <div>
                <h4>{this.state.user.name}</h4>
                <p>{this.state.user.username}</p>
                <p>{this.state.user.id}</p>

                <div>
                    <h4>Personal Details</h4>
                    <p>{this.state.user.email}</p>
                    <p>{this.state.user.phone}</p>
                    <p>{this.state.user.website}</p>
                </div>
            </div>
        ) : (
            <div> User is currently loading....</div>
        )

        return(
            <div>
                { user }
            </div>
        )
    }
}

export default User