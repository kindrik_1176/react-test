



import React, { Component } from 'react';

import axios from 'axios';

import { Link } from 'react-router-dom';

class App extends Component {
  constructor() {
    super();
    this.state = {
      users: []
    }
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(result => {

        this.setState({
          users: result.data
        })
      })
  }

  render() {

    const { users } = this.state;

    const userList = users.length ? (

      users.map(user => {
        return (
          <div key = { user.id }>
            <div className = 'data-item'>
              <Link to={'/' + user.id}>
                <span>{ user.name }</span>
              </Link>
              <p>{ user.phone }</p>
              <p>{ user.email }</p>
            </div>
          </div>
        )
      })

    ) : (
      <div>No users</div>
    )

    return (
      <div>
        { userList }
      </div>
    );
  }
}

export default App;